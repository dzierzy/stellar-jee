package stellar.web.faces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class LocaleBean {

    private String selectedLocale =
            FacesContext.
                    getCurrentInstance().
                    getViewRoot().
                    getLocale()
            .getLanguage();


    public String getSelectedLocale() {
        return selectedLocale;
    }

    public void setSelectedLocale(String selectedLocale) {
        FacesContext.
                getCurrentInstance().
                getViewRoot().
                setLocale( new Locale(selectedLocale) );
        this.selectedLocale = selectedLocale;
    }


    public List<Locale> getSupportedLocales(){
        Iterator<Locale> itr =
                FacesContext.
                        getCurrentInstance().
                        getApplication().
                        getSupportedLocales();
        List<Locale> locales = new ArrayList<>();
        itr.forEachRemaining(l->locales.add(l));

        locales.add(FacesContext.
                getCurrentInstance().
                getApplication().
                getDefaultLocale()
        );

        return locales;
    }


}
