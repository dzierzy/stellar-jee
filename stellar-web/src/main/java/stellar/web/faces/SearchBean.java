package stellar.web.faces;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.logging.Logger;

@ManagedBean
public class SearchBean implements Serializable {

    Logger logger = Logger.getLogger(SearchBean.class.getName());

    private String phrase;

    private int systemId;

    public SearchBean(){
        logger.severe("search bean constructed!");
    }

    @PostConstruct
    public void initSystemId(){
        String systemIdString =
                FacesContext.
                        getCurrentInstance().
                        getExternalContext().
                        getRequestParameterMap().
                        get("systemId");
        if(systemIdString!=null && !systemIdString.trim().isEmpty()){
            this.systemId = Integer.parseInt(systemIdString);
        }
    }

    public boolean isPhrasePresent(){
        return phrase!=null && !phrase.trim().isEmpty();
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getSystemId() {
        return systemId;
    }

    public void setSystemId(int systemId) {
        this.systemId = systemId;
    }


}
