package stellar.web.faces;

import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;
import stellar.service.impl.StellarServiceImpl;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@ManagedBean(name="stellarBean")
public class StellarBean {

    //@ManagedProperty("#{stellarService}")

    @Inject
    private Logger logger/* = Logger.getLogger(StellarBean.class.getName())*/;

    @Inject
    private StellarService service;

    @ManagedProperty("#{searchBean}")
    private SearchBean searchBean;


    public List<PlanetarySystem> getPlanetarySystems() {
        logger.info("about to search planetary systems");
        if (searchBean.isPhrasePresent()) {
            return service.getSystemsByName(searchBean.getPhrase());
        } else {
            return service.getSystems();
        }
    }

    public PlanetarySystem getPlanetarySystem(int systemId) {
        return service.getSystemById(systemId);
    }

    public List<Planet> getPlanets() {

        if (searchBean.isPhrasePresent()) {
            return service.getPlanets(service.getSystemById(searchBean.getSystemId()), searchBean.getPhrase());
        } else {
            return service.getPlanets(service.getSystemById(searchBean.getSystemId()));
        }
    }


    public SearchBean getSearchBean() {
        return searchBean;
    }

    public void setSearchBean(SearchBean searchBean) {
        this.searchBean = searchBean;
    }
}
