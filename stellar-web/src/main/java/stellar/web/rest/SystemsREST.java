package stellar.web.rest;

import stellar.config.Timeframed;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@Path("/systems")
@Timeframed
public class SystemsREST {

    @Inject
    Logger logger;

    @Inject
    StellarService stellarService;

    @GET
    public List<PlanetarySystem> getSystems(
            @QueryParam("phrase") String phrase
            /*@HeaderParam("someHeader") String someHeader,
            @CookieParam("JSESSIONID") String sessionId,
            HttpHeaders headers*/
            ){
        logger.info("about to fetch all planetary systems");
        //logger.info("someHeader=" + someHeader + ", JSESSIONID: " + sessionId);
        List<PlanetarySystem> systems = phrase==null ? stellarService.getSystems() : stellarService.getSystemsByName(phrase);

        return systems;
                //Response.ok(systems).header("custom-header", "some value").build();
    }

    @GET
    @Path("/{id}")
    public PlanetarySystem getSystemById(@PathParam("id") int systemId){
        return stellarService.getSystemById(systemId);
    }

    @GET
    @Path("/{id}/planets")
    public List<Planet> gtePlanetsBySystem(@PathParam("id") int systemId){
        logger.info("about to fetch planets of system " + systemId);
        if(systemId>100){
            throw new IllegalArgumentException("system id illegal");
        }
        return stellarService.getPlanets(stellarService.getSystemById(systemId));
    }

    @POST
    @Path("/{id}/planets")
    public void addPlanet(@javax.validation.constraints.Min(1) @PathParam("id") int systemId, @Valid Planet planet){
        logger.info("about to add planet of system " + systemId);

        PlanetarySystem system = stellarService.getSystemById(systemId);
        planet = stellarService.addPlanet(planet, system);
    }

}
