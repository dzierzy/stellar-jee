
var MainView = React.createClass({
    render: function () {

        return (
            <div>
                <header>
                    <nav>
                        <ul>
                            <li><a>Stellar Catalogue</a></li>
                        </ul>
                    </nav>
                </header>

                <Search />

                <footer>
                    <span>reactjs</span>
                </footer>
            </div>
        );
    }
});

var Search = React.createClass({

    getInitialState: function () {
        return { loaded: false };
    },

    search: function (e) {

        var xhttp = new XMLHttpRequest();
        xhttp.open("GET",  "../webapi/systems?phrase=" + this.textInput.value, false);
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);

        this.setState({
            loaded: true,
            data: data
        });

    },

    render: function () {
        return (
            <article>

                <div id="search">
                    <span>Search</span>
                    <div className="controls">
                        <form className="searchform" role="search" method="get" action=".">
                            <div className="surname">
                                <div className="search-container">
                                    <input formControlName="name" id="searchText" autofocus type="search" className="search-input" name="name"  ref={(input) => this.textInput = input} ></input>
                                    <button type="button" role="button" value="" className="search-submit" name="submit" title="Search"
                                            onClick={this.search}  ></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <section>
                    <Systems display={this.state.loaded} data={this.state.data} />
                </section>
            </article>

        );
    }

});

var Systems = React.createClass({

    getInitialState: function () {
        return { selected: false };
    },

    select: function(id){
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET",  "../webapi/systems/" + id + "/planets", false);
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.send();
        var data = JSON.parse(xhttp.responseText);
        this.setState({
            selected: true,
            data: data
        });
    },

    render: function () {

        if (!this.props.display) {
            return null;
        }

        var that = this;

        return (

            <div>

                <div className="data">
                    <span>Planetary Systems</span>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Star</th>
                                <th>Distance</th>
                            </tr>
                        </thead>

                        <tbody>
                        { this.props.data.map(function (c) {
                            return <tr id={c.id} onClick={() => that.select(c.id)}>
                                <td>{c.name}</td>
                                <td>{c.star}</td>
                                <td>{c.distance}</td>
                            </tr>;
                        })}
                        </tbody>
                    </table>
                </div>
                <Planets display={this.state.selected}  data={this.state.data}/>
            </div>

        );
    }

});

var Planets = React.createClass({

    render: function () {

        if (!this.props.display) {
            return null;
        }

        return (

            <div className="data">
                <span>Planets</span>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                    </thead>

                    <tbody>
                    { this.props.data.map(function (c) {
                        return <tr id={c.id}>
                            <td>{c.id}</td>
                            <td>{c.name}</td>
                        </tr>;
                    })}
                    </tbody>
                </table>
            </div>

        );
    }

});


ReactDOM.render(<MainView />, document.getElementById("greeting"));






