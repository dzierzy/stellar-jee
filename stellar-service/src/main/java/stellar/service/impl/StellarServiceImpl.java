package stellar.service.impl;

import stellar.config.Timeframed;
import stellar.dao.PlanetDAO;
import stellar.dao.SystemDAO;
import stellar.dao.impl.jpa.DBBased;
import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;
import stellar.service.api.StellarServiceRemote;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.*;
import java.util.List;
import java.util.logging.Logger;



@Timeframed
@Stateless
@Local(StellarService.class)
@Remote(StellarServiceRemote.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
public class StellarServiceImpl implements StellarService {



    @Inject
    Logger logger /*= Logger.getLogger(StellarServiceImpl.class.getName())*/;

    @Inject @DBBased
    private SystemDAO systemDAO;

    @Inject @DBBased
    private PlanetDAO planetDAO;

   /* @Inject
    public StellarServiceImpl(SystemDAO systemDAO, PlanetDAO planetDAO) {
        this.systemDAO = systemDAO;
        this.planetDAO = planetDAO;
    }*/

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        return planetDAO.getPlanetsPage(s, pageNo, pageSize);
    }

    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
        TransactionSynchronizationRegistry tsr = null;
        try {
            tsr = (TransactionSynchronizationRegistry)
                    new InitialContext().lookup("java:comp/TransactionSynchronizationRegistry");
            logger.info("transaction: id[" + tsr.getTransactionKey() + "] status[" + tsr.getTransactionStatus() + "]");
        } catch (NamingException e) {
            e.printStackTrace();
        }
            PlanetarySystem system = systemDAO.addPlanetarySystem(s);
            return system;


    }


    public void setPlanetDAO(PlanetDAO planetDAO) {
        this.planetDAO = planetDAO;
    }

    public void setSystemDAO(SystemDAO systemDAO) {
        this.systemDAO = systemDAO;
    }
}
