package stellar.service.impl;

import stellar.entities.PlanetarySystem;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@MessageDriven(name = "Investigator", activationConfig = {
        @ActivationConfigProperty(propertyName="messagingType", propertyValue = "MessageListener"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")/*,
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/SystemsQueue")*/
})
public class Investigator implements MessageListener {

    @Inject
    Logger logger;

    @Override
    public void onMessage(Message message) {

        try {
            logger.info("planetary system for investigation: " + message.getBody(PlanetarySystem.class));
        } catch (JMSException e) {
            throw new RuntimeException(e);
        }
    }
}
