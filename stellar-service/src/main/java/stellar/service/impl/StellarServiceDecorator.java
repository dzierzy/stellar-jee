package stellar.service.impl;

import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarService;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Decorator
public class StellarServiceDecorator implements StellarService {

    @Inject @Delegate
    private StellarService stellarService;

    @Override
    public List<PlanetarySystem> getSystems() {
        return stellarService.getSystems();
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        return stellarService.getSystemsByName(like);
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        return stellarService.getSystemById(id);
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        List<Planet> planets = stellarService.getPlanets(s);
        planets = new ArrayList<>(planets);
        planets.add(new Planet(10, "X", s));
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return stellarService.getPlanets(system, like);
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        return stellarService.getPlanetsPage(s, pageNo, pageSize);
    }

    @Override
    public Planet getPlanetById(int id) {
        return stellarService.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        return stellarService.addPlanet(p, s);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
        return stellarService.addPlanetarySystem(s);
    }
}
