package stellar.service.impl;

import stellar.config.REBinding;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarException;
import stellar.service.api.StellarService;
import stellar.service.api.StellarWizard;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.TransactionSynchronizationRegistry;
import java.lang.IllegalStateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

@Stateful(name="StellarWizardEJB")
@Remote(StellarWizard.class)
@REBinding
@TransactionManagement(TransactionManagementType.CONTAINER)
public class StellarWizardEJB implements StellarWizard {

    @Inject
    Logger logger;

    @Inject
    private StellarService stellarService;

    private PlanetarySystem system;

    @Resource(name = "jms/CF")
    private ConnectionFactory cf;

    @Resource(name = "jms/SystemsQueue")
    private Queue queue;

    @Override
    public void createPlanetarySystem(String name, String star, float distance) throws StellarException{

        logger.info("about to create system. name=" + name + ", star=" + star + ", distance=" + distance);

        if(system!=null){
            throw new IllegalStateException("already in state of creation");
        }
        system = new PlanetarySystem(0, name, star, new Date(), distance);
        system.setPlanets(new ArrayList<>());

    }

    @Override
    public void addPlanet(String name) throws StellarException {
        logger.info("about to add new planet: " + name);

        if(system==null){
            throw new IllegalStateException("add planetary system first");
        }

        Planet planet = new Planet(0, name, system);
        system.getPlanets().add(planet);

    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public PlanetarySystem commit() throws StellarException {
        //throw new StellarException("checking...");
        TransactionSynchronizationRegistry tsr = null;
        try {
            tsr = (TransactionSynchronizationRegistry)
                    new InitialContext().lookup("java:comp/TransactionSynchronizationRegistry");
            logger.info("transaction: id[" + tsr.getTransactionKey() + "] status[" + tsr.getTransactionStatus() + "]");
        } catch (NamingException e) {
            e.printStackTrace();
        }
        PlanetarySystem ps = stellarService.addPlanetarySystem(system);
        notify(ps);
        system = null;
        return ps;
    }

    private void notify(PlanetarySystem s){
        try {
         /*   Session session = cf.createConnection().createSession();
            ObjectMessage message = session.createObjectMessage(s);
            session.createProducer(queue).send(message);
           */
            cf.createContext().createProducer().send(queue, s);

        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
