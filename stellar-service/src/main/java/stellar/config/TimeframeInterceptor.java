package stellar.config;

import stellar.config.Property;
import stellar.config.Timeframed;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.time.LocalTime;
import java.util.logging.Logger;

@Interceptor
@Timeframed
public class TimeframeInterceptor {

    @Inject
    Logger logger;

    @Inject
    @Property
    private int opening = 10;

    @Inject
    @Property
    private int closing = 17;

    @Inject
    private Event<String> event;


    @AroundInvoke
    @AroundTimeout
    public Object checkTimeframe(InvocationContext ctx) throws Exception {

        logger.info("about to check if hour is between " + opening + " and " + closing);
        int hour = LocalTime.now().getHour();
        if(hour>=opening && hour<closing ){
            return ctx.proceed();
        } else {
            // TODO fire an event
            event.fire("someone wanted to enter the system at " + hour);
            throw new RuntimeException("the system is closed :(");
        }
    }


}
