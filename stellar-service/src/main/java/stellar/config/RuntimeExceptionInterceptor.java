package stellar.config;

import stellar.service.api.StellarException;

import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@REBinding
public class RuntimeExceptionInterceptor {

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception{
        try {
            return context.proceed();
        }catch (RuntimeException e){
            throw new StellarException("intercepted", e);
        }
    }
}
