package stellar.config;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

public class LoggerProducer {

    @Produces
    public Logger getLogger(InjectionPoint ip){

        String category = ip.getMember().getDeclaringClass().getName();
        return Logger.getLogger(category);
    }

}
