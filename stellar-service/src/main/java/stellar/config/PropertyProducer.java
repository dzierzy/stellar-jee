package stellar.config;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyProducer {

    private Properties props;


    @Produces
    @Property
    public int produceInt(InjectionPoint ip){

        return Integer.parseInt(props.getProperty(
                ip.getMember().getName()
        ));
    }

    @Produces
    @Property
    public Object produceObject(InjectionPoint ip){

        return props.getProperty(
                ip.getMember().getName()
        );
    }

    @PostConstruct
    private void init(){
        props = new Properties();
        try(InputStream is = this.getClass().getResourceAsStream("/config.properties")) {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
