package stellar.config;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import java.util.Set;
import java.util.logging.Logger;

public class NotificationReceiver {

    @Inject
    Logger logger;

    @Inject
    BeanManager beanManager;


    void receiveEvent(@Observes String notification){
        logger.info("notification received: " + notification);

        Set<Bean<?>> beans = beanManager.getBeans(Object.class,new AnnotationLiteral<Any>() {});
        for (Bean<?> bean : beans) {
            logger.info(bean.getBeanClass().getName() + ", " + bean.getName());
        }
    }

}
