package stellar;

import stellar.entities.Planet;
import stellar.service.api.StellarService;
import stellar.service.impl.StellarServiceImpl;

import java.util.List;

public class StellarServiceStart {

    public static void main(String[] args) {
        System.out.println("Let's explore!");

        StellarService service = null;

        List<Planet> planets = service.getPlanets(service.getSystemById(1));

        planets.forEach(p-> System.out.println(p));

    }
}
