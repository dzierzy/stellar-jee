CREATE TABLE PLANETARY_SYSTEM(
   ID INT PRIMARY KEY AUTO_INCREMENT,
   NAME VARCHAR(255),
   STAR  VARCHAR(255),
   DISTANCE LONG,
   DISCOVERY DATE
);

CREATE TABLE PLANET(
   ID INT PRIMARY KEY AUTO_INCREMENT,
   NAME VARCHAR(255),
   SYSTEM_ID INT,
   FOREIGN KEY(SYSTEM_ID ) REFERENCES PLANETARY_SYSTEM(id)
);



insert into planetary_system (id,name,star,distance,discovery) values (1,'Solar System','Sun',1.11,'1212-02-13');
insert into planetary_system (id,name,star,distance,discovery) values (2,'Kepler-90 System','Kepler-90',2.567,'2012-06-23');
insert into planetary_system (id,name,star,distance,discovery) values (3,'Trappist-1 System','Trappist-1',13.121,'2016-01-12');
insert into planetary_system (id,name,star,distance,discovery) values (4,'Kepler-11 System','Kepler-11',7.61,'2019-02-13');




insert into planet(id,name,system_id) values (1,'Mercury',1);
insert into planet(id,name,system_id) values (2,'Venus',1);
insert into planet(id,name,system_id) values (3,'Earth',1);
insert into planet(id,name,system_id) values (4,'Mars',1);
insert into planet(id,name,system_id) values (5,'Jupiter',1);
insert into planet(id,name,system_id) values (6,'Saturn',1);
insert into planet(id,name,system_id) values (7,'Neptun',1);
insert into planet(id,name,system_id) values (8,'Uranus',1);
insert into planet(id,name,system_id) values (9,'Pluto',1);


insert into planet(id,name,system_id) values (21,'Abra',2);
insert into planet(id,name,system_id) values (22,'Kadabra',2);
insert into planet(id,name,system_id) values (23,'Hokus',2);
insert into planet(id,name,system_id) values (24,'Pokus',1);

insert into planet(id,name,system_id) values (31,'Hejkum',3);
insert into planet(id,name,system_id) values (32,'Kejkum',3);

insert into planet(id,name,system_id) values (41,'Lelum',4);
insert into planet(id,name,system_id) values (42,'Polelum',4);
