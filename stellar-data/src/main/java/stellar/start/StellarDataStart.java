package stellar.start;

import stellar.entities.PlanetarySystem;
import stellar.dao.SystemDAO;
import stellar.dao.impl.inmemory.InMemorySystemDAO;

import javax.enterprise.inject.Vetoed;
import java.util.List;

@Vetoed
public class StellarDataStart {

    public static void main(String[] args) {
        System.out.println("StellarDataStart.main");

        SystemDAO systemDAO = new InMemorySystemDAO();
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();

        systems.forEach( s -> System.out.println(s) );


    }
}
