package stellar.dao.impl.jpa;

import stellar.dao.PlanetDAO;
import stellar.entities.Page;
import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@DBBased
public class JPAPlanetDAO implements PlanetDAO {

    @PersistenceContext(unitName = "stellarUnit")
    private EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return em.createQuery("select p from Planet p where p.system=:s")
                .setParameter("s", system).getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return null;
    }

    @Override
    public Page<Planet> getPlanetsPage(PlanetarySystem s, int pageNo, int pageSize) {
        List<Planet> planets =  em.createQuery("select p from Planet p where p.system=:s")
                .setParameter("s", s).setFirstResult((pageNo-1)*pageSize).setMaxResults(pageSize).getResultList();;
        int total = em.createQuery("select count(p) from Planet p where p.system=:s").setParameter("S", s).getFirstResult();

        return new Page<>(planets, total, pageNo, pageSize);

    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        em.persist(p);
        return p;
    }
}
