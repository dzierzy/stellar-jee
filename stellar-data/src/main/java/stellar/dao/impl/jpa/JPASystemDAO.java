package stellar.dao.impl.jpa;

import stellar.dao.SystemDAO;
import stellar.entities.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@DBBased
public class JPASystemDAO implements SystemDAO {

    @PersistenceContext(unitName = "stellarUnit")
    private EntityManager em;

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return em.createQuery("select ps from PlanetarySystem ps").getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em.createQuery("select ps from PlanetarySystem ps where ps.name like concat('%',:phrase,'%')")
                .setParameter("phrase", like).getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return em.find(PlanetarySystem.class, id);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
