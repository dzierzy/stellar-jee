package stellar.client;




import stellar.entities.Planet;
import stellar.entities.PlanetarySystem;
import stellar.service.api.StellarServiceRemote;
import stellar.service.api.StellarWizard;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Date;
import java.util.List;


public class StellarClient {

    public static void main(String[] args) throws Exception {
try {
    Context ctxt = new InitialContext();

    // Adding
    System.out.println("about to add newly discovered planetary system");
    String stellarWizardJNDI = "stellar-1.0/stellar-service/StellarWizardEJB!stellar.service.api.StellarWizard";

    StellarWizard wizard = (StellarWizard) ctxt.lookup(stellarWizardJNDI);

    wizard.createPlanetarySystem("Fake system", null, 666.66f);
    wizard.addPlanet("Planet1");
    wizard.addPlanet("Planet2");
    wizard.addPlanet("Planet3");
        /*PlanetarySystem system = wizard.commit();
        System.out.println("created: " + system);
*/
    StellarWizard wizard2 = (StellarWizard) ctxt.lookup(stellarWizardJNDI);

    wizard2.createPlanetarySystem("Fake system", "Fake star", 666.66f);

    wizard2.addPlanet("Planet1");
    wizard2.addPlanet("Planet2");
    wizard2.addPlanet("Planet3");
    PlanetarySystem system = wizard2.commit();
    System.out.println("created: " + system);


    // listing
    String stellarServiceJNDI =
            "stellar-1.0/stellar-service/StellarServiceImpl!stellar.service.api.StellarServiceRemote";


    StellarServiceRemote stellarService = (StellarServiceRemote) ctxt.lookup(stellarServiceJNDI);

    List<PlanetarySystem> systems = stellarService.getSystems();
    System.out.println(systems);

}catch (Exception e){
    e.printStackTrace();
}


    }

}






