package stellar.client;

import stellar.entities.PlanetarySystem;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

public class StellarRestClient {

    public static void main(String[] args) {


        List<PlanetarySystem> system =
                 ClientBuilder.newClient().
                        target("http://localhost:8080/stellar/webapi/systems").
                        request(MediaType.APPLICATION_JSON).
                        get(List.class);

        System.out.println(system);

    }

}
