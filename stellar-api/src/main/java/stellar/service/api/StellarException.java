package stellar.service.api;

public class StellarException extends Exception {

    public StellarException(String message) {
        super(message);
    }

    public StellarException(String message, Throwable cause) {
        super(message, cause);
    }
}
