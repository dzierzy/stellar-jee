package stellar.service.api;

import stellar.entities.PlanetarySystem;

public interface StellarWizard {

    void createPlanetarySystem(String name, String star, float distance) throws StellarException;

    void addPlanet(String name) throws StellarException;

    PlanetarySystem commit() throws StellarException;

}
