package stellar.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="PLANETARY_SYSTEM")
public class PlanetarySystem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @NotNull
    private String star;

    @Temporal(value = TemporalType.DATE)
    private Date discovery;

    private float distance;

    @OneToMany(mappedBy = "system", fetch = FetchType.EAGER)
    private List<Planet> planets;

    public PlanetarySystem(int id, String name, String star, Date discovery, float distance) {
        this.id = id;
        this.name = name;
        this.star = star;
        this.discovery = discovery;
        this.distance = distance;

    }

    public PlanetarySystem(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    public Date getDiscovery() {
        return discovery;
    }

    public void setDiscovery(Date discovery) {
        this.discovery = discovery;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "PlanetarySystem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", star='" + star + '\'' +
                ", planets=" + planets +
                '}';
    }
}
